#!/usr/bin/node
const {
  bsv,
  buildContractClass,
  getPreimage,
  toHex,
  SigHashPreimage,
  Ripemd160,
  Sig,
  Bytes,
  buildTypeClasses,
  Int,
  Sha256,
  signTx,
} = require('scryptlib');

const {
  loadDesc,
  padLeadingZero
} = require('./helper');
const { COINBASETX, header, COINBASETX_FAKE_HEIGHT, merklePathOfNotLastTx, Last_TX_ID,
  NotLastTxID, merklePath, merklePathOfLastTx, merklePathOfLastTxCopy, buildMerkleProof, toBlockHeader, headers } = require('./tests/js/blockchainhelper');

const releaseFolder = './release_out';
const fs = require('fs');

function getFiles(dir, files_) {
  files_ = files_ || [];
  var files = fs.readdirSync(dir);
  for (var i in files) {
    var name = dir + '/' + files[i];
    if (fs.statSync(name).isDirectory()) {
      getFiles(name, files_);
    } else {
      files_.push(name);
    }
  }
  return files_.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });
}
const privateKeyX = new bsv.PrivateKey('5JSg9ajY2WChJTqNZZz2mPWRz8UhZ6knoxV8aaWDaoy6GxEkoh5')
const publicKeyX = bsv.PublicKey.fromPrivateKey(privateKeyX);
const pkhX = bsv.crypto.Hash.sha256ripemd160(publicKeyX.toBuffer());
const pubKeyHash = new Ripemd160(toHex(pkhX));
console.log({publicKeyX})
console.log(publicKeyX.toAddress()) // 1oLyqYzGsWfYTTLVjhfmbhgAfLrQLhWh8

// 00000032b2be2675655b69ab67ab59740fa0ae6c22539d0a5d1d4f179e5ef918
// c1073034f4aba0e4079e2c96383a46e53907d5b7e37cbb3418876cae1c45667f

const fundingTxId = 'c1073034f4aba0e4079e2c96383a46e53907d5b7e37cbb3418876cae1c45667f'
/*
./bitcoind getrawtransaction c1073034f4aba0e4079e2c96383a46e53907d5b7e37cbb3418876cae1c45667f 1
{
    "hex" : "01 00 00 00 01 53 6c c3 b9 e8 5d 57 bb 6e b2 ec 3b b1 4d e5 f3 6a 80 d1 31 e6 d1 f4 43 3f 41 cd 04 e5 bf a3 01 00 00 00 00 49 48 30 45 02 20 57 ca 80 b9 af b5 92 6a 1e 0e 5f 17 d4 e8 28 07 b2 80 c3 bf 57 14 e9 8a 4c 86 1c 90 69 67 1a d2 02 21 00 af d8 bf 89 97 69 f7 07 bc 9e 0c 29 39 82 4f 51 f2 aa 5c ee f9 12 c8 c8 7d 67 84 65 cf fd 20 1a 01 ff ff ff ff 02 80 66 ab 13 00 00 00 00 19 76 a9 14 08 c3 d0 0a c9 1a 34 ea b2 7b 5f 4e 38 c3 d3 8e c2 03 42 ac 88 ac 80 8b 5a 16 01 00 00 00 19 76 a9 14 45 03 51 17 1c b9 52 75 58 aa 1c 9d 74 9d f3 4e f8 f4 8f a8 88 ac 00 00 00 00",
    "txid" : "c1073034f4aba0e4079e2c96383a46e53907d5b7e37cbb3418876cae1c45667f",
    "version" : 1,
    "locktime" : 0,
    "vin" : [
        {
            "txid" : "01a3bfe504cd413f43f4d1e631d1806af3e54db13becb26ebb575de8b9c36c53",
            "vout" : 0,
            "scriptSig" : {
                "asm" : "0x011A20FDCF6584677DC8C812F9EE5CAAF2514F8239290C9EBC07F7699789BFD8AF002102D21A6769901C864C8AE91457BFC380B20728E8D4175F0E1E6A92B5AFB980CA5720024530",
                "hex" : "48 30 45 02 20 57 ca 80 b9 af b5 92 6a 1e 0e 5f 17 d4 e8 28 07 b2 80 c3 bf 57 14 e9 8a 4c 86 1c 90 69 67 1a d2 02 21 00 af d8 bf 89 97 69 f7 07 bc 9e 0c 29 39 82 4f 51 f2 aa 5c ee f9 12 c8 c8 7d 67 84 65 cf fd 20 1a 01"
            },
            "sequence" : 4294967295
        }
    ],
    "vout" : [
        {
            "value" : 3.300000000000000,
            "n" : 0,
            "scriptPubKey" : {
                "asm" : "OP_DUP OP_HASH160 0xAC4203C28ED3C3384E5F7BB2EA341AC90AD0C308 OP_EQUALVERIFY OP_CHECKSIG",
                "hex" : "76 a9 14 08 c3 d0 0a c9 1a 34 ea b2 7b 5f 4e 38 c3 d3 8e c2 03 42 ac 88 ac"
            }
        },
        {
            "value" : 46.70000000000000,
            "n" : 1,
            "scriptPubKey" : {
                "asm" : "OP_DUP OP_HASH160 0xA88FF4F84EF39D749D1CAA587552B91C17510345 OP_EQUALVERIFY OP_CHECKSIG",
                "hex" : "76 a9 14 45 03 51 17 1c b9 52 75 58 aa 1c 9d 74 9d f3 4e f8 f4 8f a8 88 ac"
            }
        }
    ]
}
*/

const skipListLikelyPreimage = [
  'AnyoneCanSpend',
  'AdvancedCounter',
  'AdvancedTokenSale',
  'Auction',
  'BlockchainPRNG',
  'BlockTimeBet',
  'Callee',
  'Caller',
  'Clone',
  'GameOfLife',
  'Counter',
  'DummyPrescription',
  'EnforceAgentBitcoinTransfer',
  'Faucet',
  'merkleToken',
  'NonFungibleToken',
  'OptimalPushTx',
  'OracleTest',
  'P2SH',
  'RockPaperScissors',
  'StateCounter',
  'StateMap',
  'StateStruct',
  'TokenSale',
  'Token',
  'TokenSwap',
  'TuringMachine',
  'WitnessBinaryOption',
  'ArrayDemo'
];
// Various contract helper functions
// From ecAddition
/*const EC = require('elliptic').ec;
const ec = new EC('secp256k1');
const p = ec.curve.p;
function modular_divide(bn_a, bn_b, bn_m) {
  a = bn_a.mod(bn_m)
  inv = bn_b.invm(bn_m)
  return inv.mul(a).mod(bn_m)
}
function get_lambda(P1, P2) {
  // lambda - gradient of the line between P1 and P2
  // if P1 != P2:
  //    lambda = ((P2y - P1y) / (P2x - P1x)) % p
  // else:
  //    lambda = ((3 * (P1x**2) + a) / (2 * P1y)) % p
  if (P1.getX().eq(P2.getX()) && P1.getY().eq(P2.getY())) {
    let lambda_numerator = P1.getX().sqr().muln(3)
    let lambda_denominator = P1.getY().muln(2)
    return modular_divide(lambda_numerator, lambda_denominator, p)
  } else {
    let lambda_numerator = P2.getY().sub(P1.getY())
    let lambda_denominator = P2.getX().sub(P1.getX())
    return modular_divide(lambda_numerator, lambda_denominator, p)
  }
}*/

(async () => {
  console.log('Creating experiment tx')
  // We first need to get a handle on all the different custom type classes and also create various argument variables
  /* typeClasses = buildTypeClasses(buildContractClass(loadDesc('./release_out/ecAddition_release_desc.json')));
  Point = typeClasses.Point
  // P1 and P2
  k1 = new bsv.PrivateKey.fromRandom('testnet')
  k2 = new bsv.PrivateKey.fromRandom('testnet')
  P1 = k1.publicKey.point;
  P2 = k2.publicKey.point;
  let lambda = get_lambda(P1, P2)
  // P = P1 + P2
  let Px = lambda.sqr().sub(P1.getX()).sub(P2.getX()).umod(p)
  let Py = lambda.mul(P1.getX().sub(Px)).sub(P1.getY()).umod(p)
  */

  // fixedPoint 
  const [ADD, SUB, MUL, DIV, ABS] = [0, 1, 2, 3, 4]
  // fractionMath
  let {
    Fraction
  } = buildTypeClasses(buildContractClass(loadDesc('./release_out/fractionMathTest_release_desc.json')));
  const nan =  new Fraction({
    n: 0,
    d: 0
  }); // not a number*/

  // RPuzzle Init

const BN = bsv.crypto.BN;
const Point = bsv.crypto.Point;

const secret = 'This is a secret message!';
const secretHash = bsv.crypto.Hash.sha256(Buffer.from(secret));
const k = Buffer.from(secretHash);

const G = Point.getG();
const N = Point.getN();
const Q = G.mul(new BN.fromBuffer(k));
const r = Q.x.umod(N).toBuffer();
const r0 = r[0] > 127 ? Buffer.concat([Buffer.alloc(1), r]) : r;
const rhash = bsv.crypto.Hash.sha256ripemd160(r0);


// Xor Puzzle Init

// for xor with publicKeyA
const data = '9999';
const dataBuf = Buffer.from(data);
const dataBufHash = bsv.crypto.Hash.sha256(dataBuf);
const dataBufHashHex = toHex(dataBufHash);
const dataBufHashBI = BigInt('0x' + dataBufHashHex);

const data_false = '9998';
const dataBuf_false = Buffer.from(data_false);
const dataBufHash_false = bsv.crypto.Hash.sha256(dataBuf_false);
const dataBufHashHex_false = toHex(dataBufHash_false);

// for output of locking transaction
const privateKeyA = new bsv.PrivateKey.fromRandom('testnet');
const publicKeyA = privateKeyA.publicKey;
const publicKeyAHex = toHex(publicKeyA);
const publicKeyABI = BigInt('0x' + publicKeyAHex);

const publicKeyData = publicKeyAHex + dataBufHashHex;

const dataBuffer = Buffer.from(publicKeyData, 'hex');
const publicKeyDataHash = bsv.crypto.Hash.sha256(dataBuffer);
const publicKeyDataHashHex = toHex(publicKeyDataHash);

const publicKeyDataHashBI = BigInt('0x' + publicKeyDataHashHex);

const xorResult = dataBufHashBI ^ publicKeyDataHashBI;

let xorResultHex = padLeadingZero(xorResult.toString(16));

  // Now we can create the test vectors
  const contractTestVectors = [
    {
      name: 'Ackermann',
      constructor: [
        2, 1
      ],
      unlock: [5]
    },
    /*{
      name: 'AnyoneCanSpend',
      constructor: [{ name: 'pubKeyHash', type: 'Ripemd160', state: false, value: pubKeyHash }],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'outputAmount', type: 'int' }
      ]
    },
    {
      name: 'AdvancedCounter',
      constructor: [{ name: 'counter', type: 'int', state: true, value: 0 }],
      unlock: [{ name: 'txPreimage', type: 'SigHashPreimage' }]
    },
    {
      name: 'AdvancedTokenSale',
      constructor: [{ name: 'price', type: 'int', state: false }],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'changePKH', type: 'Ripemd160' },
        { name: 'changeSats', type: 'int' },
        { name: 'buyer', type: 'bytes' },
        { name: 'numTokens', type: 'int' }
      ]
    },
    {
      name: 'Auction',
      constructor: [
        { name: 'bidder', type: 'Ripemd160', state: true },
        { name: 'auctioner', type: 'PubKey', state: false },
        { name: 'auctionDeadline', type: 'int', state: false }
      ],
      unlock: [
        { name: 'bidder', type: 'Ripemd160' },
        { name: 'bid', type: 'int' },
        { name: 'changeSats', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    /*{
      name: 'BlockchainTest',
      constructor: [],
      unlock: [
        toBlockHeader(BlockHeader, header),
        buildMerkleProof(Node, merklePath), new Bytes(COINBASETX), 575191
      ]
    },
    {
      name: 'BlockchainPRNG',
      constructor: [
        { name: 'blockchainTarget', type: 'int', state: false },
        { name: 'alice', type: 'PubKey', state: false },
        { name: 'bob', type: 'PubKey', state: false }
      ],
      unlock: [
        { name: 'bh', type: 'BlockHeader' },
        { name: 'merkleproof', type: 'Node[32]' },
        { name: 'sig', type: 'Sig' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'BlockTimeBet',
      constructor: [
        { name: 'blockchainTarget', type: 'int', state: false },
        { name: 'alice', type: 'PubKey', state: false },
        { name: 'bob', type: 'PubKey', state: false }
      ],
      unlock: [
        { name: 'headers', type: 'BlockHeader[7]' },
        { name: 'merkleproof', type: 'Node[32]' },
        { name: 'sig', type: 'Sig' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'Callee',
      constructor: [],
      unlock: [
        { name: 'co', type: 'Coeff' },
        { name: 'x', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'Caller',
      constructor: [{ name: 'calleeContractHash', type: 'Ripemd160', state: false }],
      unlock: [
        { name: 'co', type: 'Coeff' },
        { name: 'prevouts', type: 'bytes' },
        { name: 'calleeContractTx', type: 'bytes' },
        { name: 'outputScript', type: 'bytes' },
        { name: 'amount', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'Clone',
      constructor: [],
      unlock: [{ name: 'txPreimage', type: 'SigHashPreimage' }]
    },
    {
      name: 'GameOfLife',
      constructor: [{ name: 'board', type: 'bytes', state: true }],
      unlock: [
        { name: 'amount', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'Counter',
      constructor: [],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'amount', type: 'int' }
      ]
    },
    {
      name: 'DummyPrescription',
      constructor: [
        { name: 'drug', type: 'int', state: false },
        { name: 'expirationDate', type: 'int', state: false },
        { name: 'prescriptionID', type: 'bytes', state: false },
        { name: 'patientReward', type: 'int', state: false },
        { name: 'patientPubKeyHash', type: 'Ripemd160', state: false },
        { name: 'pharmacyPubKeys', type: 'PubKey[2]', state: false },
        { name: 'rabinSig', type: 'RabinSig', state: false }
      ],
      unlock: [
        { name: 'pharmacySig', type: 'Sig' },
        { name: 'prescriberPubKey', type: 'int' },
        { name: 'currBlock', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'ArrayDemo',
      constructor: [],
      unlock: [{ name: '_x', type: 'int' }]
    },
    {
      name: 'ECAddition',
      constructor: [
        new Point({
          x: new Int(P1.getX().toString(10)),
          y: new Int(P1.getY().toString(10))
        }),
        new Point({
          x: new Int(P2.getX().toString(10)),
          y: new Int(P2.getY().toString(10))
        }),
      ],
      unlock: [new Int(lambda.toString(10)),
        new Point({
          x: new Int(Px.toString(10)),
          y: new Int(Py.toString(10))
        })]
    },
    {
      name: 'EnforceAgentBitcoinTransfer',
      constructor: [
        { name: 'agentKeyHash', type: 'Ripemd160', state: false },
        { name: 'approveOutputHash', type: 'Sha256', state: false },
        { name: 'refundOutputHash', type: 'Sha256', state: false },
        { name: 'expirationKeyHash', type: 'Ripemd160', state: false },
        { name: 'expiration', type: 'int', state: false }
      ],
      unlock: [
        { name: 'sig', type: 'Sig' },
        { name: 'pubKey', type: 'PubKey' },
        { name: 'preimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'Faucet',
      constructor: [],
      unlock: [
        { name: 'preImage', type: 'SigHashPreimage' },
        { name: 'depositAmount', type: 'int' },
        { name: 'changePKH', type: 'Ripemd160' },
        { name: 'changeAmount', type: 'int' }
      ]
    },*///testFractionMath?
    {
      name: 'Test',
      constructor: [],
      unlock: [ //   it('should div 1.234 with 2.345 correctly', () => {
        10**4, Math.trunc(1.234*10**4), Math.trunc(2.345*10**4), DIV, 5262
      ]
    },
    /*{
      name: 'Main',
      constructor: [],
      unlock: [
        // it('should abs -1/3 correctly', () => {
        //  x = new Fraction({n: -1, d: 3});
        new Fraction({n: -1, d: 3}), nan, new Fraction({n: 1, d: 3}), ABS, false
      ]
    },*/
    /*
  before(() => {
    HashPuzzle = buildContractClass(compileContract('hashpuzzle.scrypt'))
    hashPuzzle = new HashPuzzle(new Sha256(toHex(bsv.crypto.Hash.sha256(Buffer.from("abc")))))
    //hashPuzzle.txContext = { tx, inputIndex, inputSatoshis }
  });

  it('check should succeed when correct data provided', () => {
    result = hashPuzzle.verify(new Bytes(toHex(data))).verify()*/
    {
      name: 'HashPuzzle',
      constructor: [new Sha256(toHex(bsv.crypto.Hash.sha256(Buffer.from("abc"))))],
      unlock: [new Bytes(toHex(Buffer.from("abc")))]
    },
    {
      name: 'HashPuzzleP2PKH',
      constructor: [
        pubKeyHash,
        new Sha256(toHex(bsv.crypto.Hash.sha256(Buffer.from("abc")))),
      ],
      unlock: [
        Buffer.from("abc"),
        // { name: 'sig', type: 'Sig' },
        publicKeyX
      ]
    },
    {
      name: 'Matrix',
      constructor: [],
      unlock: [
        [10, 10, 10, 10],
        [20, 20, 20, 20],
        [30, 30, 30, 30],
        [40, 40, 40, 40]]
    },
    /*{
      name: 'merkleToken',
      constructor: [{ name: 'price', type: 'int', state: false }],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'amount', type: 'int' },
        { name: 'changePKH', type: 'Ripemd160' },
        { name: 'payoutPKH', type: 'Ripemd160' },
        { name: 'changeSats', type: 'int' },
        { name: 'prevBalance', type: 'int' },
        { name: 'merklePath', type: 'bytes' }
      ]
    },*/
    /*let merkleRoot = new Bytes("cd53a2ce68e6476c29512ea53c395c7f5d8fbcb4614d89298db14e2a5bdb5456")
    let leaf, merklePath

    leaf = new Bytes("6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b")
    merklePath = new Bytes(
      [
        "d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35",
        "01",
        "20ab747d45a77938a5b84c2944b8f5355c49f21db0c549451c6281c91ba48d0d",
        "01"
      ].join("")*/
    {
      name: 'MerkleTreeTest',
      constructor: [],
      unlock: [
        new Bytes("cd53a2ce68e6476c29512ea53c395c7f5d8fbcb4614d89298db14e2a5bdb5456"),
        new Bytes(
          [
            "d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35",
            "01",
            "20ab747d45a77938a5b84c2944b8f5355c49f21db0c549451c6281c91ba48d0d",
            "01"
          ].join("")),
          new Bytes("cd53a2ce68e6476c29512ea53c395c7f5d8fbcb4614d89298db14e2a5bdb5456"),
        ]
    },
    {
      name: 'ModExp',
      constructor: [496],
      unlock: [
        4, 13, 445
      ]
    },
    /*{
      name: 'MultiSig',
      constructor: [{ name: 'pubKeyHashs', type: 'Ripemd160[3]', state: false }],
      unlock: [
        { name: 'pubKeys', type: 'PubKey[3]' },
        { name: 'sigs', type: 'Sig[3]' }
      ]
    },*/
    /*{
      name: 'NonFungibleToken',
      constructor: [],
      unlock: [
        { name: 'issuerSig', type: 'Sig' },
        { name: 'receiver', type: 'PubKey' },
        { name: 'satoshiAmount0', type: 'int' },
        { name: 'satoshiAmount1', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },/*
    {
      name: 'OptimalPushTx',
      constructor: [],
      unlock: [{ name: 'txPreimage', type: 'SigHashPreimage' }]
    },*/
    /*{
      name: 'OracleTest',
      constructor: [{ name: 'oraclePubKey', type: 'PubKey', state: false }],
      unlock: [
        { name: 'data', type: 'bytes' },
        { name: 'sig', type: 'Sig' },
        { name: 'derivedOraclePubKey', type: 'PubKey' },
        { name: 'X', type: 'PubKey' },
        { name: 'lambda', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },/*
    {
      name: 'DemoP2PKH',
      constructor: [{ name: 'pubKeyHash', type: 'Ripemd160', state: false }],
      unlock: [{ name: 'sig', type: 'Sig' }, { name: 'pubKey', type: 'PubKey' }]
    },
    {
      name: 'P2SH',
      constructor: [{ name: 'scriptHash', type: 'Ripemd160', state: false }],
      unlock: [
        { name: 'redeemScript', type: 'bytes' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'TestRabinSignature',
      constructor: [],
      unlock: [
        { name: 'msg', type: 'bytes' },
        { name: 'sig', type: 'RabinSig' },
        { name: 'n', type: 'int' }
      ]
    },*/
    /*{
      name: 'RockPaperScissors',
      constructor: [],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'action', type: 'int' },
        { name: 'playerBpkh', type: 'Ripemd160' },
        { name: 'satoshiAmount', type: 'int' }
      ]
    },*/
    {
      name: 'RPuzzle',
      constructor: [rhash],
      unlock: [
        { name: 'sig', type: 'Sig' },
        { name: 'pubKey', type: 'PubKey' },
        { name: 'sigr', type: 'Sig' }
      ]
    },/*
    {
      name: 'Test',
      constructor: [],
      unlock: [{ name: 'f', type: 'bool' }]
    },*/
    {
      name: 'SimpleBVM',
      constructor: [3],
      unlock: [new Bytes(toHex('525593569357936094539354935894'))]
    },
    /*{
      name: 'StateCounter',
      constructor: [{ name: 'counter', type: 'int', state: true }],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'amount', type: 'int' }
      ]
    },/*
    {
      name: 'StateMap',
      constructor: [{ name: '_mpData', type: 'bytes', state: true }],
      unlock: [
        { name: 'entry', type: 'MapEntry' },
        { name: 'preimage', type: 'SigHashPreimage' }
      ]
    },*/
    /*{
      name: 'StateStruct',
      constructor: [],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'amount', type: 'int' }
      ]
    },/*
    {
      name: 'StructDemo',
      constructor: [{ name: 'male', type: 'Person', state: false }],
      unlock: [{ name: 'person1', type: 'Person' }]
    },*/
    {
      name: 'Sudoku',
      constructor: [
        [
          [2,1,9,0,8,6,0,5,3],
          [7,4,5,9,1,0,2,8,6],
          [8,3,6,7,5,2,1,4,9],
          [0,6,3,2,7,4,9,1,8],
          [4,8,7,0,3,9,6,2,5],
          [9,2,1,5,6,8,0,7,4],
          [6,0,2,3,4,5,8,0,1],
          [3,9,0,8,2,1,5,6,7],
          [1,0,8,6,9,7,4,0,2]
        ]
      ],
      unlock: [
          [2,1,9,4,8,6,7,5,3],
          [7,4,5,9,1,3,2,8,6],
          [8,3,6,7,5,2,1,4,9],
          [5,6,3,2,7,4,9,1,8],
          [4,8,7,1,3,9,6,2,5],
          [9,2,1,5,6,8,3,7,4],
          [6,7,2,3,4,5,8,9,1],
          [3,9,4,8,2,1,5,6,7],
          [1,5,8,6,9,7,4,3,2]
      ]
    },/*
    {
      name: 'Token',
      constructor: [{ name: 'accounts', type: 'Account[2]', state: true }],
      unlock: [
        { name: 'sender', type: 'PubKey' },
        { name: 'senderSig', type: 'Sig' },
        { name: 'receiver', type: 'PubKey' },
        { name: 'value', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'amount', type: 'int' }
      ]
    },
    {
      name: 'TokenSale',
      constructor: [{ name: 'price', type: 'int', state: false }],
      unlock: [
        { name: 'buyer', type: 'PubKey' },
        { name: 'numTokens', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'TokenSwap',
      constructor: [
        { name: 'initBsv', type: 'int', state: false },
        { name: 'firstSaveBsvAmount', type: 'int', state: false },
        { name: 'firstSaveTokenAmount', type: 'int', state: false },
        { name: 'feeRate', type: 'int', state: false },
        { name: 'poolPubkey', type: 'PubKey', state: false },
        { name: 'adminPubkey', type: 'PubKey', state: false }
      ],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'userPubkey', type: 'PubKey' },
        { name: 'insertMerklePath', type: 'bytes' },
        { name: 'adminSig', type: 'Sig' }
      ]
    },/*
    {
      name: 'Token',
      constructor: [],
      unlock: [
        { name: 'senderSig', type: 'Sig' },
        { name: 'receiver0', type: 'PubKey' },
        { name: 'tokenAmount0', type: 'int' },
        { name: 'satoshiAmount0', type: 'int' },
        { name: 'receiver1', type: 'PubKey' },
        { name: 'tokenAmount1', type: 'int' },
        { name: 'satoshiAmount1', type: 'int' },
        { name: 'txPreimage', type: 'SigHashPreimage' }
      ]
    },
    {
      name: 'TuringMachine',
      constructor: [{ name: 'states', type: 'StateStruct', state: true }],
      unlock: [{ name: 'txPreimage', type: 'SigHashPreimage' }]
    },
    {
      name: 'WitnessBinaryOption',
      constructor: [
        { name: 'symbol', type: 'bytes', state: false },
        { name: 'decimal', type: 'int', state: false },
        { name: 'betPrice', type: 'int', state: false },
        { name: 'matureTime', type: 'int', state: false },
        { name: 'witnessPubKey', type: 'int', state: false },
        { name: 'pubKeyHashA', type: 'Ripemd160', state: false },
        { name: 'pubKeyHashB', type: 'Ripemd160', state: false }
      ],
      unlock: [
        { name: 'txPreimage', type: 'SigHashPreimage' },
        { name: 'price', type: 'int' },
        { name: 'unlockTime', type: 'int' },
        { name: 'outAmount', type: 'int' },
        { name: 'rabinSig', type: 'RabinSig' }
      ]
    },*/
    {
      name: 'xorPuzzle',
      constructor: [
        new Bytes(xorResultHex)],
      unlock: [
        /*{ name: 'sig', type: 'Sig' },
        { name: 'pubKey', type: 'PubKey' },
        { name: 'data', type: 'bytes' }*/
      ]
    }
  ];

  const fileList = getFiles(releaseFolder);
  const compiledOutputs = []
  fileList.
  slice(0,200).
  map(filename => {
    try{
      // console.log({filename})
      const Contract = buildContractClass(loadDesc(filename))
      // console.log({Contract})
      // Skip preimage contracts for now
      if (skipListLikelyPreimage.includes(Contract.contractName)){
        // console.log("Skipping")
        return
      }
      // console.log({ name: Contract.contractName, constructor: Contract.abi.find(x => x.type === 'constructor').params, unlock: Contract.abi.find(x => x.type === 'function' && (x.name === 'unlock' || x.index === 0)).params }, ',')
      // console.log(contractTestVectors.find(x => x.name === Contract.contractName).constructor[0])
      const contract = new Contract(...contractTestVectors.find(x => x.name === Contract.contractName).constructor.map(arg => {
        return arg
      }))
      // console.log({contract})
      let unlockingScript = undefined
      try{
        unlockingScript = toHex(contract.unlock(...contractTestVectors.find(x => x.name === Contract.contractName).unlock.map(arg => {
          return arg
        }))._unlockingScriptAsm)
      }catch(error){

      }
      if(unlockingScript){
        console.log("BUILT: "+filename,{
          lockingScript: contract.lockingScript.toHex(),
          unlockingScript
        })
        compiledOutputs.push({
          filename,
          contract,
          lockingScriptHex: contract.lockingScript.toHex(),
          unlockingScript
        })
      }
    }
    catch(error){
      // console.log(filename)
      // console.log("FAILED COMPILATION\n", error.message)
    }
  })

  console.log({compiledOutputs})
  const lockingTx = new bsv.Transaction();
  const COIN = 100000000;
  const reversedFundingTxId = Buffer.from(fundingTxId, 'hex').reverse().toString('hex')

  lockingTx.addInput(new bsv.Transaction.Input({
    prevTxId: fundingTxId,
    outputIndex: 0,
    script: bsv.Script('76a91408c3d00ac91a34eab27b5f4e38c3d38ec20342ac88ac'),
    sequenceNumber: 1, // Required for lock time
  }), '', 3.3 * COIN);

  lockingTx.addOutput(new bsv.Transaction.Output({
    script: bsv.Script.fromHex(compiledOutputs[0].lockingScriptHex),
    satoshis: 3 * COIN,
  }))

  
    // call contract method on testnet
    const unlockingTx = new bsv.Transaction();

    lockingTx.addInput(createInputFromPrevTx(lockingTx))
      .setOutput(0, (tx) => {
        const newLockingScript = bsv.Script.buildPublicKeyHashOut(privateKey.toAddress())
        return new bsv.Transaction.Output({
          script: newLockingScript,
          satoshis: amount - tx.getEstimateFee(),
        })
      })
      .setInputScript(0, (tx, output) => {
        const sig = signTx(unlockingTx, privateKey, output.script, output.satoshis)
        return p2pkh.unlock(sig, new PubKey(toHex(publicKey))).toScript()
      })
      .seal()

  console.log(lockingTx)
  console.log(lockingTx.hash)

})();
