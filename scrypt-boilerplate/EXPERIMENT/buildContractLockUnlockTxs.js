#!/usr/bin/node
const args = require('minimist')(process.argv.slice(2))
const fundingTxHex = process.argv.slice(2)[0] //joe
console.log(args, fundingTxHex)
if (!fundingTxHex) {
  console.log("Run script by ./testbroadcast fundingTxHex")
  return
}

const {
  bsv,
  toHex,
  Ripemd160,
  signTx,
  PubKey,
  buildContractClass
} = require('scryptlib');

const {
  loadDesc,
  createInputFromPrevTx,
} = require('../helper');

const Signature = bsv.crypto.Signature;
const privateKey = new bsv.PrivateKey('5JSg9ajY2WChJTqNZZz2mPWRz8UhZ6knoxV8aaWDaoy6GxEkoh5')
const publicKey = bsv.PublicKey.fromPrivateKey(privateKey);
const pkhX = bsv.crypto.Hash.sha256ripemd160(publicKey.toBuffer());
const pubKeyHash = new Ripemd160(toHex(pkhX));
console.log({ publicKey, pubKeyHash })
console.log(publicKey.toAddress()) // 1oLyqYzGsWfYTTLVjhfmbhgAfLrQLhWh8


const AckermannIO = require("./contract-io-generation/ackermann");
const AuctionIO = require('./contract-io-generation/auction');

(async () => {
  console.log('Compiling all nifty contracts experiment tx');
  const ackermann = await (new AckermannIO()).buildContractIO()
  const auction = await (new AuctionIO()).buildContractIO()
  
  const contractIOClasses = [
    ackermann,
    auction
  ]

  console.log('Creating experiment tx')

  // Parse command line rawTx arg into the funding transaction
  const fundingTx = bsv.Transaction()
  fundingTx.fromString(fundingTxHex)
  console.log(fundingTx)
  const COIN = 100000000;

  // C++ node change address index is non-deterministic
  let fundingOutputIndex = 0
  if (fundingTx.outputs[0].satoshis !== 100000000) // Hardcoded to 1 bitcoin
    fundingOutputIndex = 1

  // Funding by standard pubkeyhash so prepare its unlocking script
  const P2PKH = buildContractClass(loadDesc('./release_out/p2pkh_release_desc.json'))
  const p2pkh = new P2PKH(new Ripemd160(toHex(pubKeyHash)))

  const lockingInput = createInputFromPrevTx(fundingTx, fundingOutputIndex)
  console.log(lockingInput)
  const sighashType = Signature.SIGHASH_ALL | Signature.SIGHASH_ANYONECANPAY // | Signature.SIGHASH_FORKID;
  const lockingTx = new bsv.Transaction();

  lockingTx.addInput(lockingInput)
    .setOutput(0, (tx) => {
      const newLockingScript = bsv.Script.fromString(ackermann.lockingScriptHex)
      return new bsv.Transaction.Output({
        script: newLockingScript,
        satoshis: 0.9 * COIN,
      })
    })
    .setInputScript(0, (tx, output) => {
      const sig = signTx(lockingTx, privateKey, output.script, output.satoshis, 0, sighashType)
      return p2pkh.unlock(sig, new PubKey(toHex(publicKey))).toScript()
    })

  console.log({ sighashType })
  console.log((lockingTx.toString()))
  console.log(lockingTx.hash)

  const scryptLockingInput = createInputFromPrevTx(lockingTx, fundingOutputIndex)
  console.log(scryptLockingInput)
  // Now we need to produce the unlocking tx also
  const unlockingTx = new bsv.Transaction();

/*
  lockingTx.addInput(lockingInput)
    .setOutput(0, (tx) => {
      const newLockingScript = bsv.Script.fromString(ackermann.lockingScriptHex)
      return new bsv.Transaction.Output({
        script: newLockingScript,
        satoshis: 0.9 * COIN,
      })
    })
    .setInputScript(0, (tx, output) => {
      const sig = signTx(lockingTx, privateKey, output.script, output.satoshis, 0, sighashType)
      return p2pkh.unlock(sig, new PubKey(toHex(publicKey))).toScript()
    })

  console.log({ sighashType })
  console.log((lockingTx.toString()))
  console.log(lockingTx.hash)*/

})();
