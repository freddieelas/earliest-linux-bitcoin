# The Plan
Attempt to deploy all the scrypt contracts (current count=85) on the very first bitcoin protocol (v0.1).

## Tasks
- Attempt to deploy all 85 scrypt contracts in a single transaction using the new utxos
- Attempt to unlock the transaction contract utxos
- Profit

### Steps
Which contracts are we looking to test?
Using the test files the contracts which have test vectors were chosen to whittle the list down somewhat.
Then we can separate out the preimage dependent contracts for later and also those which needed to call buildTypeClasses are also set aside for the moment.

The idea is to create two linked transactions with many inputs and outputs demonstrating these contract types both locking and unlocking as it is the fastest route to the conclusion of the experiment.

Ultimately the following contracts were subjectively chosen to be the most interesting, given real world use-cases today:

- Ackermann
- Auction
- BlockchainPRNG
- GameOfLife
- DummyPrescription
- ECAddition
- Test
- HashPuzzle
- HashPuzzleP2PKH
- Matrix
- MerkleTreeTest
- ModExp
- NonFungibleToken
- OracleTest
- RockPaperScissors
- RPuzzle
- SimpleBVM
- Sudoku
- xorPuzzle

## How to test
In topmost level run:
sudo docker-compose build && sudo docker-compose up

Then, wait for everthing to boot up in docker output (including rpc_node log generating new genesis block) and travel to localhost/faucet.
There, once the chain has started and 2 or more blocks have been found you will be sent 100000000 satoshis to the hardcoded address "1oLyqYzGsWfYTTLVjhfmbhgAfLrQLhWh8" and you will be redirected to an error page which can be refreshed until the transaction is confirmed.
From there you can take the HEX string and run it through:
cd scrypt-boilerplate/EXPERIMENT
chmod +x buildContractLockUnlock.js 
./buildContractLockUnlock.js <faucetUtxoHexStringHere>

And you should be able to go to /localhost/broadcast and broadcast the scrypt made locking transaction! Ackermann is working now, many more to be translated and incorporated into locking & unlocking ...

## Details
Compiling all contracts in Release (log)
```
[Info  - 19:01:35] Compiled xorPuzzle.scrypt successfully, [21 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/xorPuzzle_release_desc.json
[Info  - 19:02:28] Compiled witnessBinaryOption.scrypt successfully, [960 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/witnessBinaryOption_release_desc.json
resolve array sub N fail
[Info  - 19:02:48] Compiled vault.scrypt successfully, [5373 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/vault_release_desc.json
[Info  - 19:02:50] Compiled vanityAddr.scrypt successfully, [1825 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/vanityAddr_release_desc.json
[Warn  - 19:02:54] Compiled util.scrypt successfully, but has no output due to lack of public functions.
[Warn  - 19:02:56] Compiled txUtil.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:03:00] Compiled turingMachine.scrypt successfully, [7024 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/turingMachine_release_desc.json
[Info  - 19:03:11] Compiled tsp.scrypt successfully, [16763 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/tsp_release_desc.json
[Info  - 19:03:23] Compiled treeSig.scrypt successfully, [2227 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/treeSig_release_desc.json
[Info  - 19:03:27] Compiled tokenUtxo.scrypt successfully, [3214 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/tokenUtxo_release_desc.json
[Info  - 19:03:35] Compiled tokenSwap.scrypt successfully, [25830 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/tokenSwap_release_desc.json
[Info  - 19:03:37] Compiled tokenSale.scrypt successfully, [1171 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/tokenSale_release_desc.json
[Info  - 19:03:39] Compiled token.scrypt successfully, [3358 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/token_release_desc.json
[Info  - 19:03:41] Compiled timedcommit.scrypt successfully, [47 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/timedcommit_release_desc.json
[Info  - 19:03:44] Compiled testUtil.scrypt successfully, [1079 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/testUtil_release_desc.json
[Info  - 19:03:55] Compiled svd.scrypt successfully, [33779 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/svd_release_desc.json
[Info  - 19:03:57] Compiled SuperAssetNFT.scrypt successfully, [182 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/SuperAssetNFT_release_desc.json
[Info  - 19:03:59] Compiled SuperAsset10.scrypt successfully, [2382 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/SuperAsset10_release_desc.json
[Info  - 19:04:39] Compiled sudoku.scrypt successfully, [132956 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/sudoku_release_desc.json
[Info  - 19:04:41] Compiled structdemo.scrypt successfully, [220 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/structdemo_release_desc.json
[Info  - 19:04:43] Compiled stateStruct.scrypt successfully, [2097 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/stateStruct_release_desc.json
[Info  - 19:04:46] Compiled stateMap.scrypt successfully, [6767 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/stateMap_release_desc.json
[Info  - 19:04:47] Compiled statecounter.scrypt successfully, [1485 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/statecounter_release_desc.json
[Info  - 19:04:49] Compiled spvToken.scrypt successfully, [1432 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/spvToken_release_desc.json
[Info  - 19:04:57] Compiled simpleBVM.scrypt successfully, [3838 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/simpleBVM_release_desc.json
[Info  - 19:05:00] Compiled serializerTest.scrypt successfully, [4182 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/serializerTest_release_desc.json
[Warn  - 19:05:02] Compiled serializer.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:05:28] Compiled schnorr.scrypt successfully, [141737 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/schnorr_release_desc.json
[Info  - 19:05:31] Compiled rule110.scrypt successfully, [1874 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/rule110_release_desc.json
[Info  - 19:05:32] Compiled rpuzzle.scrypt successfully, [40 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/rpuzzle_release_desc.json
[Info  - 19:05:35] Compiled rps.scrypt successfully, [2509 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/rps_release_desc.json
[Info  - 19:05:36] Compiled recurring.scrypt successfully, [2354 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/recurring_release_desc.json
[Info  - 19:05:38] Compiled rabinTest.scrypt successfully, [55 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/rabinTest_release_desc.json
[Warn  - 19:05:39] Compiled rabin.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:05:41] Compiled perceptron2.scrypt successfully, [1153 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/perceptron2_release_desc.json
[Info  - 19:05:47] Compiled perceptron.scrypt successfully, [13439 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/perceptron_release_desc.json
[Info  - 19:05:49] Compiled p2sh.scrypt successfully, [820 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/p2sh_release_desc.json
[Info  - 19:05:50] Compiled p2sh.scrypt successfully, [820 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/p2sh_release_desc.json
[Info  - 19:05:52] Compiled p2pkh.scrypt successfully, [12 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/p2pkh_release_desc.json
[Info  - 19:05:54] Compiled p2nftpkh.scrypt successfully, [5 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/p2nftpkh_release_desc.json
[Info  - 19:05:56] Compiled oracleTest.scrypt successfully, [1855 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/oracleTest_release_desc.json
[Warn  - 19:05:58] Compiled oracle.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:05:59] Compiled optimalPushtx.scrypt successfully, [89 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/optimalPushtx_release_desc.json
[Info  - 19:06:01] Compiled OCSPreimage.scrypt successfully, [1678 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/OCSPreimage_release_desc.json
[Info  - 19:06:04] Compiled nonFungibleToken.scrypt successfully, [2153 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/nonFungibleToken_release_desc.json
[Info  - 19:06:05] Compiled netflix.scrypt successfully, [24 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/netflix_release_desc.json
[Info  - 19:06:07] Compiled multiSig.scrypt successfully, [25 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/multiSig_release_desc.json
[Info  - 19:06:12] Compiled modExp.scrypt successfully, [8848 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/modExp_release_desc.json
[Info  - 19:06:17] Compiled merkleTreeTest.scrypt successfully, [10715 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/merkleTreeTest_release_desc.json
[Warn  - 19:06:19] Compiled merkleTree2.scrypt successfully, but has no output due to lack of public functions.
[Warn  - 19:06:20] Compiled merkleTree.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:06:25] Compiled merkleToken.scrypt successfully, [13303 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/merkleToken_release_desc.json
[Warn  - 19:06:27] Compiled merklePath.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:06:34] Compiled matrix.scrypt successfully, [22499 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/matrix_release_desc.json
[Info  - 19:06:36] Compiled mast.scrypt successfully, [2825 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/mast_release_desc.json
[Info  - 19:06:38] Compiled lottery.scrypt successfully, [405 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/lottery_release_desc.json
[Info  - 19:06:51] Compiled kaggle.scrypt successfully, [42855 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/kaggle_release_desc.json
[Info  - 19:06:52] Compiled hashpuzzlep2pkh.scrypt successfully, [39 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/hashpuzzlep2pkh_release_desc.json
[Info  - 19:06:54] Compiled hashpuzzle.scrypt successfully, [6 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/hashpuzzle_release_desc.json
[Info  - 19:06:55] Compiled fractionMathTest.scrypt successfully, [933 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/fractionMathTest_release_desc.json
[Info  - 19:06:57] Compiled fractionMathTest.scrypt successfully, [933 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/fractionMathTest_release_desc.json
[Warn  - 19:06:58] Compiled fractionMath.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:07:00] Compiled forward.scrypt successfully, [1155 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/forward_release_desc.json
[Info  - 19:07:02] Compiled fixedPointTest.scrypt successfully, [129 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/fixedPointTest_release_desc.json
[Info  - 19:07:06] Compiled final_loop_main.scrypt successfully, [4976 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/final_loop_main_release_desc.json
[Warn  - 19:07:08] Compiled final_loop_library.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:07:11] Compiled faucet.scrypt successfully, [2397 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/faucet_release_desc.json
[Info  - 19:07:13] Compiled enforceAgentBitcoinTransfer.scrypt successfully, [478 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/enforceAgentBitcoinTransfer_release_desc.json
[Info  - 19:07:31] Compiled ecdsa.scrypt successfully, [95585 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/ecdsa_release_desc.json
[Info  - 19:07:32] Compiled ecAddition.scrypt successfully, [400 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/ecAddition_release_desc.json
[Warn  - 19:07:34] Compiled ec.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:07:36] Compiled dynamicArrayDemo.scrypt successfully, [3522 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/dynamicArrayDemo_release_desc.json
[Info  - 19:07:37] Compiled dynamicArray.scrypt successfully, [15 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/dynamicArray_release_desc.json
[Info  - 19:07:39] Compiled dummy_prescription.scrypt successfully, [913 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/dummy_prescription_release_desc.json
[Info  - 19:07:46] Compiled deadMansSwitchStatefull.scrypt successfully, [18671 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/deadMansSwitchStatefull_release_desc.json
[Info  - 19:07:54] Compiled deadMansSwitchStatefull.scrypt successfully, [18671 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/deadMansSwitchStatefull_release_desc.json
[Info  - 19:07:55] Compiled counter.scrypt successfully, [988 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/counter_release_desc.json
[Info  - 19:08:02] Compiled conwaygol.scrypt successfully, [19498 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/conwaygol_release_desc.json
[Info  - 19:08:03] Compiled cointossxor.scrypt successfully, [85 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/cointossxor_release_desc.json
[Info  - 19:08:04] Compiled cointoss.scrypt successfully, [50 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/cointoss_release_desc.json
[Info  - 19:08:06] Compiled cltvOCS.scrypt successfully, [642 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/cltvOCS_release_desc.json
[Info  - 19:08:07] Compiled cltv.scrypt successfully, [641 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/cltv_release_desc.json
[Info  - 19:08:09] Compiled clone.scrypt successfully, [988 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/clone_release_desc.json
[Info  - 19:08:11] Compiled caller.scrypt successfully, [2224 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/caller_release_desc.json
[Info  - 19:08:13] Compiled callee.scrypt successfully, [989 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/callee_release_desc.json
[Info  - 19:08:15] Compiled bns.scrypt successfully, [565 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/bns_release_desc.json
[Info  - 19:08:18] Compiled blockTimeBet.scrypt successfully, [6724 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/blockTimeBet_release_desc.json
[Info  - 19:08:20] Compiled blockPRNG.scrypt successfully, [3235 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/blockPRNG_release_desc.json
[Info  - 19:08:25] Compiled blockchainTest.scrypt successfully, [14298 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/blockchainTest_release_desc.json
resolve array sub N fail
[Warn  - 19:08:27] Compiled blockchain.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:08:28] Compiled binaryOption.scrypt successfully, [951 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/binaryOption_release_desc.json
[Warn  - 19:08:30] Compiled base58.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:08:32] Compiled auction.scrypt successfully, [2999 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/auction_release_desc.json
[Info  - 19:08:33] Compiled asm.scrypt successfully, [106 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/asm_release_desc.json
[Warn  - 19:08:35] Compiled arrayUtil.scrypt successfully, but has no output due to lack of public functions.
[Info  - 19:08:37] Compiled arraydemo.scrypt successfully, [3535 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/arraydemo_release_desc.json
[Info  - 19:08:39] Compiled array.scrypt successfully, [2622 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/array_release_desc.json
[Info  - 19:08:41] Compiled advancedTokenSale.scrypt successfully, [1256 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/advancedTokenSale_release_desc.json
[Info  - 19:08:43] Compiled advancedCounter.scrypt successfully, [1521 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/advancedCounter_release_desc.json
[Info  - 19:08:45] Compiled acs.scrypt successfully, [833 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/acs_release_desc.json
[Info  - 19:08:46] Compiled ackermann.scrypt successfully, [1777 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/ackermann_release_desc.json
[Info  - 19:08:48] Compiled accumulatorMultiSig.scrypt successfully, [337 bytes], output was saved to /home/lol/code/earliest-linux-bitcoin/out/accumulatorMultiSig_release_desc.json
```

