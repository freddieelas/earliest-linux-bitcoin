#!/usr/bin/node
const args = require('minimist')(process.argv.slice(2))
const fundingTxHex = process.argv.slice(2)[0] //joe
console.log(args,fundingTxHex)
if(!fundingTxHex){
  console.log("Run script by ./testbroadcast fundingTxHex")
  return
}
const {
  bsv,
  buildContractClass,
  toHex,
  Ripemd160,
  signTx,
  PubKey
} = require('scryptlib');

const {
  loadDesc,
  createInputFromPrevTx,
} = require('./helper');

const Signature = bsv.crypto.Signature;
  
const privateKey = new bsv.PrivateKey('5JSg9ajY2WChJTqNZZz2mPWRz8UhZ6knoxV8aaWDaoy6GxEkoh5')
const publicKey = bsv.PublicKey.fromPrivateKey(privateKey);
const pkhX = bsv.crypto.Hash.sha256ripemd160(publicKey.toBuffer());
const pubKeyHash = new Ripemd160(toHex(pkhX));
console.log({ publicKey })
console.log(publicKey.toAddress()) // 1oLyqYzGsWfYTTLVjhfmbhgAfLrQLhWh8

console.log('Creating experiment tx')
const lockingTx = bsv.Transaction()
lockingTx.fromString(fundingTxHex)
console.log(lockingTx)
const COIN = 100000000;

// call contract method on testnet
const unlockingTx = new bsv.Transaction();


  let lockingOutputIndex = 0
  if(lockingTx.outputs[0].satoshis !== 100000000) 
  lockingOutputIndex = 1
  const lockingInput = createInputFromPrevTx(lockingTx,lockingOutputIndex)

console.log(lockingInput)

    // Initialize contract
    const P2PKH = buildContractClass(loadDesc('./release_out/p2pkh_release_desc.json'))
    const p2pkh = new P2PKH(new Ripemd160(toHex(pubKeyHash)))

    const sighashType = Signature.SIGHASH_ALL | Signature.SIGHASH_ANYONECANPAY // | Signature.SIGHASH_FORKID;

    unlockingTx.addInput(lockingInput)
      .setOutput(0, (tx) => {
        const newLockingScript = bsv.Script.buildPublicKeyHashOut(privateKey.toAddress())
        return new bsv.Transaction.Output({
          script: newLockingScript,
          satoshis: 0.9*COIN,
        })
      })
      .setInputScript(0, (tx, output) => {
        const sig = signTx(unlockingTx, privateKey, output.script, output.satoshis, 0, sighashType)
        return p2pkh.unlock(sig, new PubKey(toHex(publicKey))).toScript()
      })
      
      console.log({sighashType})
console.log((unlockingTx.toString()))
console.log(unlockingTx.hash)
