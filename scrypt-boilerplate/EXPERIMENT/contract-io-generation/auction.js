const ContractIO = require("./ContractIO")

const {  bsv, toHex,  Ripemd160, PubKey } = require('scryptlib');
const inputSatoshis = 100000;

const privateKeyHighestBid = new bsv.PrivateKey.fromRandom('testnet');
const publicKeyHighestBid = bsv.PublicKey.fromPrivateKey(privateKeyHighestBid);
const publicKeyHashHighestBid = bsv.crypto.Hash.sha256ripemd160(publicKeyHighestBid.toBuffer());
const addressHighestBid = privateKeyHighestBid.toAddress();
const privateKeyAuctioner = new bsv.PrivateKey.fromRandom('testnet');
const publicKeyAuctioner = bsv.PublicKey.fromPrivateKey(privateKeyAuctioner);
const privateKeyNewBid = new bsv.PrivateKey.fromRandom('testnet');
const publicKeyNewBid = bsv.PublicKey.fromPrivateKey(privateKeyNewBid);
const publicKeyHashNewBid = bsv.crypto.Hash.sha256ripemd160(publicKeyNewBid.toBuffer());
const addressNewBid = privateKeyNewBid.toAddress();
const bid = inputSatoshis + 10000;
const FEE = 5000;
const payinputSatoshis = 200000;
const changeSats = payinputSatoshis - bid - FEE;
const onedayAgo = new Date("2022-01-26");
const auctionDeadline = Math.round( onedayAgo.valueOf() / 1000 );

/*
const preimage = getPreimage(tx, auction.lockingScript, inputSatoshis)

auction.txContext = {
    tx,
    inputIndex,
    inputSatoshis
}
*/
class AuctionIO extends ContractIO {
    constructor(){
       super('auction_release_desc.json', {
        name: 'Auction',
        constructor: [
          /*{ name: 'bidder', type: 'Ripemd160', state: true },
          { name: 'auctioner', type: 'PubKey', state: false },
          { name: 'auctionDeadline', type: 'int', state: false }*/
          new Ripemd160(toHex(publicKeyHashHighestBid)), new PubKey(toHex(publicKeyAuctioner)), auctionDeadline
        ],
        unlock: [/*
          { name: 'bidder', type: 'Ripemd160' },
          { name: 'bid', type: 'int' },
          { name: 'changeSats', type: 'int' },
          { name: 'txPreimage', type: 'SigHashPreimage' }*/
          new Ripemd160(toHex(publicKeyHashNewBid)), bid, changeSats, undefined //new SigHashPreimage(toHex(preimage))
        ],
        unlockFunctionName: 'bid'
      });
    }
   
    async buildContractIO(){
        console.log('AckermannIO.buildContractIO');
        return new Promise(resolve => {
            resolve(super.buildContractIO());
        })
     }
 }
 module.exports = AuctionIO;
 