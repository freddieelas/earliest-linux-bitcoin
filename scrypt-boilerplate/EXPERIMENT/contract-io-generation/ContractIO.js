const {
    buildContractClass,
    toHex,
} = require('scryptlib');

const {
    loadDesc,
} = require('../../helper');

class ContractIO {
    constructor(filename, args) {
        console.log('ContractIO', filename, args)
        this.filename = filename
        this.args = args
        this.contract = undefined
        this.lockingScriptHex = undefined
        this.unlockingScript = undefined
    }

    async buildContractIO() {
        try {
            console.log('ContractIO.buildContractIO', this.filename, this.args)
            const Contract = buildContractClass(loadDesc('./release_out/' + this.filename))
            this.contract = new Contract(...this.args.constructor)
            this.lockingScriptHex = this.contract.lockingScript.toHex()
            return this
        }
        catch (error) {
            console.log(this.filename)
            console.log("FAILED COMPILATION\n", error.message)
            return error
        }
    }

    async unlockContractIO(txPreimage = undefined, txContext = undefined) {
        console.log('ContractIO.unlockContractIO', this.filename, this.args)
        try {
            if (txPreimage) {
                // Assuming first 'undefined' arg is preimage get its index
                var idx = this.args.indexOf(undefined)
                // Make a copy and replace it with the supplied value
                const unlockArgs = []
                this.args.map((arg, idx) => {
                    unlockArgs.push(arg)
                })
                if (idx !== -1) { unlockArgs[idx] = txPreimage }
            }
            if (txContext)
                this.contract.txContext = txContext
            // Now try unlock the contract
            this.unlockingScript = toHex(this.contract.unlock(...unlockArgs.map(arg => {
                return arg
            }))._unlockingScriptAsm)
        } catch (error) {
            console.log("Could not UNLOCK")
        }
    }
}


module.exports = ContractIO;