const ContractIO = require("./ContractIO")

class AckermannIO extends ContractIO {
    constructor(){
       super('ackermann_release_desc.json', {
        name: 'Ackermann',
        constructor: [
          2, 1
        ],
        unlock: [5]
      });
    }
   
    async buildContractIO(){
        console.log('AckermannIO.buildContractIO');
        return new Promise(resolve => {
            resolve(super.buildContractIO());
        })
     }
 }
 module.exports = AckermannIO;
 