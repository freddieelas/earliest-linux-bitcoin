from fastapi import FastAPI, Request
import os, sys
app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}

async def run_command(command):
    # print("run_command",file=sys.stdout)
    # print(command,file=sys.stdout)
    data = os.popen(command).read()

    # p = subprocess.Popen(str(command), shell=True, stdout=subprocess.PIPE)
    # (output, err) = p.communicate()  

    #This makes the wait possible
    # p_status = p.wait()

    #This will give you the output of the command being executed
    print ("Command output: " + str(data))
    return data

async def print_request(request):
    print(f'request header       : {dict(request.headers.items())}' )
    print(f'request query params : {dict(request.query_params.items())}')  
    try : 
        print(f'request json         : {await request.json()}')
    except Exception as err:
        # could not parse json
        print(f'request body         : {await request.body()}')
        
@app.post('/cmd')
async def command_server(request: Request):
    data = await request.json()
    # print(data,file=sys.stdout)
    whitelist_cmds = ['getblockchaininfo','getblockhash','getblock','getrawtransaction','sendtoaddress','sendrawtransaction']
    if(data['method'] in whitelist_cmds):
        # print(" ".join(map(str, data['params'])))
        formatted = "../bitcoin-0.3.0/bitcoind "+str(data['method'])+" "+str(" ".join(map(str, data['params'])))
        # print("TRYING COMMAND: "+formatted)
        result = await run_command(formatted)
        return result
    else:
        #return "OK" #run_command(request)
        return "COMMAND NOT IMPLEMENTED"