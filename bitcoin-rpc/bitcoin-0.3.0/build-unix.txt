Copyright (c) 2009-2010 Satoshi Nakamoto
Distributed under the MIT/X11 software license, see the accompanying
file license.txt or http://www.opensource.org/licenses/mit-license.php.
This product includes software developed by the OpenSSL Project for use in
the OpenSSL Toolkit (http://www.openssl.org/).  This product includes
cryptographic software written by Eric Young (eay@cryptsoft.com).


UNIX BUILD NOTES
================

Dependencies
------------
sudo apt-get install build-essential
sudo apt-get install libgtk2.0-dev
sudo apt-get install libssl-dev
sudo apt-get install libdb4.7-dev
sudo apt-get install libdb4.7++-dev
sudo apt-get install libboost-all-dev

We're now using wxWidgets 2.9, which uses UTF-8.

There isn't currently a debian package of wxWidgets we can use.  The 2.8
packages for Karmic are UTF-16 unicode and won't work for us, and we've had
trouble building 2.8 on 64-bit.

You need to download wxWidgets from http://www.wxwidgets.org/downloads/
and build it yourself.  See the build instructions and configure parameters
below.


Licenses of statically linked libraries:
wxWidgets      LGPL 2.1 with very liberal exceptions
Berkeley DB    New BSD license with additional requirement that linked software must be free open source
Boost          MIT-like license

Versions used in this release:
GCC          4.4.3
OpenSSL      0.9.8k
wxWidgets    2.9.0
Berkeley DB  4.7.25.NC
Boost        1.40.0


Notes
-----
The UI layout is edited with wxFormBuilder.  The project file is
uiproject.fbp.  It generates uibase.cpp and uibase.h, which define base
classes that do the rote work of constructing all the UI elements.

The release is built with GCC and then "strip bitcoin" to strip the debug
symbols, which reduces the executable size by about 90%.


wxWidgets
---------
// https://bugs.launchpad.net/kicad-winbuilder/+bug/1019889
// GCC 4.7 installed by mingw-get cannot compile wxWidgets 2.9.2 - so pumped wxWidgets to 2.9.4
wget https://github.com/wxWidgets/wxWidgets/releases/download/v2.8.12/wxWidgets-2.8.12.tar.gz

cd /usr/local
tar -xzvf wxWidgets-2.9.5.tar.gz
cd /usr/local/wxWidgets-2.9.5
mkdir buildgtk
cd buildgtk
../configure --with-gtk --enable-debug --disable-shared --enable-monolithic --disable-std_string
make -j8
sudo make install && sudo ldconfig

cd ..
mkdir buildbase
cd buildbase
../configure --disable-gui --enable-debug --disable-shared --enable-monolithic LIBS=-lX11
make -j8
sudo make install && sudo ldconfig

Boost
-----
If you want to build Boost yourself,
cd /usr/local/boost_1_40_0
su
./bootstrap.sh
./bjam install

mkdir obj
mdkir obj/nogui
make -f makefile.unix bitcoind

ld -lwx_baseu-2.9 --verbose

