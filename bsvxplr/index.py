#!/usr/bin/env python3

# from bsvxplr.api import RPC, RPC_USER, RPC_PASS, RPC_HOST, RPC_PORT
from bsvxplr import rpc

genesis_block_hash = "00000013468029829aa49013b208590ffd87ce3b9a260f84f4dbb519b4acdf1b"
print(rpc.getblock(genesis_block_hash, 2))