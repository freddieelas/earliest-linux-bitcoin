# bsvxplr
Bitcoin Block Explorer

## Requirements
1. Install required packages with pip
```
pip3 install -r requirements.txt
```

2. [A fully synced Bitcoin Full Node running the bitcoind service](https://bitcoin.org/en/full-node) installed in the same system or in any network that is reachable from your host.

3. `BTC_RPC_USER` and `BTC_RPC_PASS` must be present in the system environment and contains the correct credentials to make RPC calls to your bitcoin full node.

4. Ports for HTTP, HTTPS, and RPC should be allowed in your firewall rules.
Example for `ufw`
```
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 8332:8333
```

## Deployment
### Flask
```
python3 run.py
```

### Gunicorn
```
gunicorn --bind 0.0.0.0:5000 run:app
```

### Gunicorn + Nginx
Create a unit file ending in .service within the `/etc/systemd/system` directory
```
sudo vim /etc/systemd/system/bsvxplr.service
```

Sample contents of unit file 
```
[Unit]
#  specifies metadata and dependencies
Description=Gunicorn instance to serve bsvxplr

# tells the init system to only start this after the networking target has been reached
After=network.target

[Service]
# Service specify the user and group under which our process will run.
User=<replace with username>

# Give group ownership to the www-data group so that Nginx can communicate easily with the Gunicorn processes.
Group=www-data

# Specify the working directory and set the PATH environmental variable so that the init system knows where our the executables for the process are located (within our virtual environment).
WorkingDirectory=<replace with path to bsvxplr>
Environment="PATH=<replace with path to bsvxplr virtual environment>"

# Specify the commanded to start the service
# Rule of thumb for number of workers is twice the number of cores plus one
# Example for a dual core system, the ideal number of workers is five (5).
ExecStart=<replace with path to gunicorn> --workers <replace with number of workers> --bind unix:app.sock -m 007 run:app

[Install]
# This will tell systemd what to link this service to if we enable it to start at boot. We want this service to start when the regular multi-user system is up and running:
WantedBy=multi-user.target
```

Start the Gunicorn service we created
```
sudo systemctl start bsvxplr
```

Enable the service so that it starts at boot
```
sudo systemctl enable bsvxplr
```

A new `app.sock` file will be created inside the bsvxplr directory automatically

Create a new server block configuration file in Nginx’s sites-available directory named `bsvxplr`
```
sudo vim /etc/nginx/sites-available/bsvxplr
```

Open up a server block in which nginx will listen to `PORT 80`. This block will also be used for requests for the server’s domain name or IP address
```
server {
    listen 80;
    server_name <replace with server ip or domain name>;
}
```

Add a location block that matches every request. In this block, include the `proxy_paramsfile` that specifies some general proxying parameters that need to be set. Then pass the requests to the socket defined earlier using the `proxy_pass` directive
```
server {
    listen 80;
    server_name <replace with server ip or domain name>;

    location / {
      include proxy_params;
      # replace with path to bsvxplr app.sock file
      proxy_pass http://unix:/home/username/bsvxplr/app.sock;
        }
}
```

Create a symbolic link inside the `sites-enabled` directory to enable Nginx server block just created.
```
sudo ln -s /etc/nginx/sites-available/bsvxplr /etc/nginx/sites-enabled/bsvxplr
```

Test syntax errors by typing
```
sudo nginx -t
``` 

If there is no issues, restart the Nginx process to load new config
```
sudo systemctl restart nginx
```

If firewall rules are not yet updated to allow `HTTP` and `HTTPS`.
Example for `ufw`
```
sudo ufw allow 'Nginx Full'
```

You may now access `bsvxplr` by keying your ip address or domain name in your web browser.
```
http://server_domain_or_ip_address
```

##### Optional HTTPS
To enable `HTTPS` using certificates, you can use [Certbot](https://certbot.eff.org/) and create a `HTTP 301` redirect from `PORT 80` to `PORT 443` in your server configuration in nginx.

Example server block configuration
```
server {
  server_name <your_server_domain>;

  location / {
    include proxy_params;
    # replace with path to bsvxplr app.sock file
    proxy_pass http://unix:/home/username/bsvxplr/app.sock;
  }

  listen 443 ssl; # managed by Certbot
  ssl_certificate /etc/letsencrypt/live/<your_server_domain>/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/<your_server_domain>/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = <your_server_domain>) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    listen 80;
    server_name <your_server_domain>;
    return 404; # managed by Certbot
}
```

You may now securely access `bsvxplr` by keying your ip address or domain name in your web browser.
```
https://server_domain_or_ip_address
```
Keying in the url with `http://server_domain_or_ip_address` will automatically redirect users to `https://server_domain_or_ip_address`.
Starting Nmap 7.80 ( https://nmap.org ) at 2022-01-17 22:00 GMT
Nmap scan report for 206.189.30.55
Host is up (0.024s latency).
Not shown: 995 closed ports
PORT     STATE    SERVICE
22/tcp   open     ssh
1186/tcp filtered mysql-cluster
2701/tcp filtered sms-rcinfo
6547/tcp filtered powerchuteplus
9877/tcp filtered unknown

Nmap done: 1 IP address (1 host up) scanned in 57.32 seconds
ian@ian-HP-EliteBook-820-G3:~$ nmap 206.189.30.55
Starting Nmap 7.80 ( https://nmap.org ) at 2022-01-17 22:01 GMT
Nmap scan report for 206.189.30.55
Host is up (0.0000070s latency).
Not shown: 922 filtered ports, 77 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 28.48 seconds

docker inspect