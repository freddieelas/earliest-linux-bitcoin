#!/usr/bin/env python3
'''
This is where the routes are defined. It may be split into a package of its own (yourapp/views/) with related views grouped together into modules.
'''
from ast import Return
from bsvxplr import app, rpc
from bsvxplr.api import RPC, RPC_USER, RPC_PASS, RPC_HOST, RPC_PORT, get_coindesk_price
from flask import render_template, redirect, url_for, flash, request, Response
from datetime import datetime
import json
from kaitaistruct import KaitaiStream, BytesIO
from .bitcoin_transaction import BitcoinTransaction
SighashType = BitcoinTransaction.Vin.ScriptSignature.SighashType

@app.route('/')
def index():
    # peers = rpc.getpeerinfo()
    return render_template(
            'index.html',
            # peers=peers
        )

"""@app.route('/node')
def get_nodeinfo():
    # peers = rpc.getpeerinfo()
    return render_template(
            'node.html',
            # peers=peers
        )"""

@app.route('/stats')
def get_stats():
    mining = rpc.getmininginfo()
    return render_template(
            'stats.html',
            data=mining
        )
"""
@app.route('/pool', defaults={'txid': None})
@app.route('/pool/<string:txid>')
def list_mempool(txid):
    if txid:
        rtx = rpc.getrawtransaction(txid, verbose=True)
        confirmed = True if 'confirmations' in rtx else False
        # if block is mined
        if confirmed:
            flash('Transaction already in blockchain', 'primary')
            return redirect(url_for('get_txn', txid=txid))
            
        # if still in mempool
        else:
            data = rpc.getmempoolentry(txid)
            # if valid txid
            if data:
                return render_template(
                    'pooltx.html',
                    data=data, rtx=rtx
                )
            # flash warning and redirect if invalid txid
            else:
                flash('Invalid Transaction ID', 'warning')
                return redirect(url_for('list_mempool'))
    else:
        pool = rpc.getrawmempool()
        return render_template(
            'pool.html',
            data=pool
        )"""

@app.route('/broadcast', methods=['GET', 'POST'])
def broadcast():
    if request.method == 'POST':
        print(request.values)
        # username = request.values.get('user') # Your form's
        # password = request.values.get('pass') # input names
        print("POST")
        print(request.data)
        print(type(request.data))
        rawtx = request.data.decode('utf-8')
        
            
        print("Attemping send")
        result = rpc.sendrawtransaction(rawtx)
        print(type(result))
        print(result)

        """print("HERE")
        print(result)
        print(type(result))
        for key, value in result.items() :
            print (key, value)
        print(result['txid'])
        print(result['txid']=="Transaction rejected\n")"""

        #    return redirect(url_for('get_txn', txid=txid))
    # build txn data from genesis block data
        # your_register_routine(username, password)
        # return {"NOTHING YET"}
        """return render_template(
            'error.html',
            data=result
        )"""
        if 'error' in result.keys():
            # if(result['error']=="Transaction rejected"):
            print("REJECTED")
            flash('Transaction rejected', 'secondary')
            rawtxbytes = bytearray.fromhex(rawtx)
            data = BitcoinTransaction(KaitaiStream(BytesIO(rawtxbytes)))
            for vin in data.vins:
                print("VIN sighashtype: "+str(vin.script_sig.sig_type))
                print(vin.script_sig.sig_type)
                try:
                    validSigHash = SighashType(vin.script_sig.sig_type)
                except:
                    print("OH SHIT sighashtype INCORRECT")
                    result['error']+= ": Incorrect sigHashType please use original"
            return render_template(
                'error.html',
                data=result
            )
        else: 
            print("ACCEPTED")
            flash('Transaction sent', 'primary')
            return result
    else:
        # You probably don't have args at this route with GET
        # method, but if you do, you can access them like so:
        # yourarg = request.args.get('argname')
        # your_register_template_rendering(yourarg)
        print("GET")
        return render_template(
            'broadcast.html',
            #data=pool
        )

from hashlib import sha256

digits58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'

def decode_base58(bc, length):

    n = 0

    for char in bc:

        n = n * 58 + digits58.index(char)

    return n.to_bytes(length, 'big')

def check_bc(bc):
    print("check_bc")
    print(bc)
    bcbytes = decode_base58(bc, 25)

    return bcbytes[-4:] == sha256(sha256(bcbytes[:-4]).digest()).digest()[:4]


@app.route('/faucet', methods=['GET', 'POST'])
def faucet():
    if request.method == 'POST':
        print("POST")
        print(request.data)
        print(type(request.data))
        address = request.data.decode('utf-8')
        print(address)
        # username = request.values.get('user') # Your form's
        # password = request.values.get('pass') # input names
        print("POST")

        try:
            assert check_bc(address)
        except:
            # your_register_routine(username, password)
            return render_template(
                'error.html',
                data="INVALID ADDRESS"
            )
        print("Attemping send")
        result = rpc.sendtoaddress(address)#, verbosity=2)
        print(type(result))
        print(result)

        flash('Transaction sent', 'primary')
        #    return redirect(url_for('get_txn', txid=txid))
    # build txn data from genesis block data
        # your_register_routine(username, password)
        # return {"NOTHING YET"}
        """return render_template(
            'error.html',
            data=result
        )"""
        return result
    else:
        # You probably don't have args at this route with GET
        # method, but if you do, you can access them like so:
        # yourarg = request.args.get('argname')
        # your_register_template_rendering(yourarg)
        print("GET")
        return render_template(
            'faucet.html',
            #data=pool
        )

@app.route('/block/<int:blockid>', methods=['GET', 'POST'])
def get_block(blockid):
    blockchain_info = rpc.getblockchaininfo()
    print("blockchain_info")
    print(blockchain_info)
    print(type (blockchain_info))
    # flash waring and redirect if blockid is less than genesis block or more than current block height 
    if blockid > blockchain_info['blocks'] or blockid < 0:
        flash('Invalid Block Height', 'warning')
        return redirect(url_for('get_stats'))

    print('blcokid: ', blockid)

    block_hash = rpc.getblockhash(blockid)
    print('block_hash: ', block_hash)
    block_data = rpc.getblock(block_hash)#, verbosity=2)
    print(block_data)
    try:
        block_data['miner'] = rpc.getrawtransaction(block_data['tx'][0], verbose=True)['vout'][0]['scriptPubKey']['addresses'][0]
    except:
        block_data['miner'] = None

    # try:
    #    block_stats = rpc.getblockstats(blockid)
    # except:
    #    block_stats = {}

    return render_template(
        'block.html',
        data=block_data,
        # stats=block_stats,
        info=blockchain_info,
    )


@app.route('/tx/<string:txid>', methods=['GET', 'POST'])
def get_txn(txid):
    # special case for genesis block
    """if txid == '4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b':
        data=build_genesis_txn()

    else:"""
    print(txid)
    data = rpc.getrawtransaction(txid, verbose=True)
    print("DAT: "+str(data))
    print(type(data))
    if 'code' in data.keys():#['vin']:
        print("SHOULD BE ERRORING")
        return render_template(
            'error.html',
            data=data
        )
    try:
        data['height'] = rpc.getblock(data['blockhash'])['height']
    except:
        data['height'] = None
    if 'vin' in data.keys():#['vin']:
        for n, txin in enumerate(data['vin']):
            try:
                txin_data = rpc.getrawtransaction(txin['txid'], verbose=True)
                data['vin'][n]['value'] =  txin_data['vout'][txin['vout']]['value']
                # data['vin'][n]['address'] =  txin_data['vout'][n]['scriptPubKey']['addresses'][0]
                data['vin'][n]['scriptPubKey'] = txin_data['vout'][txin['vout']]['scriptPubKey']
            except:
                data['vin'][n]['value'] = None
                data['vin'][n]['scriptPubKey'] = None

    # if 'vout' in data.keys():#['vin']:
    #    for n, txout in enumerate(data['vout']):
    #        data['vout'][n]['spent'] = False if rpc.gettxout(txid, n) else True
    data['hex'] = data['hex'].replace(' ','')
    return render_template(
        'txn.html',
        data=data
    )


@app.route('/addr/<string:address>', methods=['GET', 'POST'])
def get_addr(address):
    raw_tx = rpc.getaddressinfo(address)
    return render_template(
        'index.html',
        data=raw_tx
    )

### Error Handlers ###
@app.errorhandler(404)
def page_not_found(e):
    return redirect(url_for('index'))

### Templates ###
@app.template_filter('ctime')
def timectime(s):
    return datetime.fromtimestamp(s)

### Auxiliary Functions ###
def build_genesis_txn():
    genesis_block_hash = '000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f'
    block_data = rpc.getblock(genesis_block_hash, verbosity=2)

    # build txn data from genesis block data
    data = dict()
    data['txid'] = block_data['tx'][0]['txid']
    data['hash'] = block_data['tx'][0]['hash']
    data['version'] = block_data['tx'][0]['version']
    data['size'] = block_data['tx'][0]['size']
    data['version'] = block_data['tx'][0]['version']
    data['vsize'] = block_data['tx'][0]['vsize']
    data['weight'] = block_data['tx'][0]['weight']
    data['locktime'] = block_data['tx'][0]['locktime']
    data['vin'] = block_data['tx'][0]['vin']
    data['vout'] = block_data['tx'][0]['vout']
    data['hex'] = block_data['tx'][0]['hex']
    data['blockhash'] = block_data['hash']
    data['confirmations'] = block_data['confirmations']
    data['time'] = block_data['time']
    data['blocktime'] = block_data['time']

    return data